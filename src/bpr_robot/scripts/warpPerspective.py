#!/usr/bin/env python
import rospy
from std_msgs.msg import Float64
from std_msgs.msg import String
from sensor_msgs.msg import Image
import cv2
from cv_bridge import CvBridge, CvBridgeError
import numpy as np

bridge = CvBridge()

def rotate_image(image, angle):
  image_center = tuple(np.array(image.shape[1::-1]) / 2)
  rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1.0)
  result = cv2.warpAffine(image, rot_mat, image.shape[1::-1], flags=cv2.INTER_LINEAR)
  return result

# Rotates opencv image based on the angle of joint 1 of the robot
def callback(image, angle):
    try:
        # convert ros image to opencv image
        cv_image = bridge.imgmsg_to_cv2(image, "passthrough")
        if angle.data >= 0.8:
            # rotate image by 45 degrees if angle is greater than or equal to 0.8
            cv_image = rotate_image(cv_image, 45)
        elif angle.data < 0.8 and angle.data >= 0.5:
            cv_image = rotate_image(cv_image, 25)
        elif angle.data < -0.5 and angle.data >= -0.8:
            cv_image = rotate_image(cv_image, -25)
        elif angle.data < -0.8:
            cv_image = rotate_image(cv_image, -45)
        else:
            cv_image = rotate_image(cv_image, 0)
        # cv2.imshow("image", cv_image)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()
    except CvBridgeError, e:
        rospy.logerr("CvBridge Error: {0}".format(e))

    # Create a topic to publish to
    alignedImage = rospy.Publisher("imageToFindLetters", Image, queue_size=10)

    try:
        # Convert opencv image back to ros image to publish
        image = bridge.cv2_to_imgmsg(cv_image, "bgr8")
        # Publish rotated image to /imageToFindLetters
        alignedImage.publish(image)
    except CvBridgeError as e:
        print(e)

# Subscribes to /cameraTip/image_raw
# Get image and angle and pass to the callback function, callback
# Angle is passed in by rotateImage()
def warp(angle):
    # To make sure the robot has stopped swaying
    rospy.sleep(5)
    # To pass in additional argument to callback, add another parameter to Subscriber - angle
    rospy.Subscriber("/cameraTip/image_raw", Image, callback, angle)

# Subscribes to /joint1_position_controller/command
# When new joint1 angle is published, get the float angle and run the callback function, warp
def rotateImage(): 
    rospy.Subscriber("/joint1_position_controller/command", Float64, warp)
    rospy.spin()

if __name__ == '__main__':
    rospy.init_node('rotateImageNode', anonymous=True)
    rotateImage()
