#!/usr/bin/env python
import rospy
from sensor_msgs.msg import Image
import cv2
import numpy as np
from cv_bridge import CvBridge, CvBridgeError
from predictLetter import predict_Letter
bridge = CvBridge()

def callback(data):
    try:

        # convert ros image to opencv image
        cv_image = bridge.imgmsg_to_cv2(data, "passthrough")
        gray = cv2.cvtColor(cv_image, cv2.COLOR_BGR2GRAY)

        #Input image should always be grayscale
        #If pixel is below/above 128, assign it 255.
        #cv2.THRESH_BINARY is the retrieval method
        #cv2.threshold outputs two values. First is retVal, second is the actual image. 
        #We want to grab the actual image - [1]
        thresh = cv2.threshold(gray,128,255,cv2.THRESH_BINARY)[1]

        result = cv_image.copy()

        contours = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        contours = contours[0] if len(contours) == 2 else contours[1]

        ROI_number = 0
        for cntr in contours:
            x,y,w,h = cv2.boundingRect(cntr)
            ROI = result[y:y+h, x:x+w]

            # make a prediction on each ROI
            prediction = predict_Letter(ROI)
            # if prediction == 'f':
            #     break

            # show content of each ROI bounding box
            cv2.imshow("ROI", ROI)
            cv2.waitKey(0)
            cv2.destroyAllWindows()
            cv2.rectangle(result, (x,y), (x+w, y+h), (0,0,255), 2)

        # show the whole image with all bounding boxes present
        cv2.imshow("bounding box", result)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
    except CvBridgeError, e:
        rospy.logerr("CvBridge Error: {0}".format(e))


def readKeysInView():
    # subscribing to image that is processed and published by warpPerspective.py
    rospy.Subscriber("/imageToFindLetters", Image, callback)
    rospy.spin()

if __name__ == '__main__':
    rospy.init_node('boundingBoxNode', anonymous=True)
    readKeysInView()