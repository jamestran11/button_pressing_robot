#!/usr/bin/env python

import rospy
from std_msgs.msg import Float64
from predictLetter import predict_Letter

def talker():
    pubJoint2 = rospy.Publisher('/joint2_position_controller/command', Float64, queue_size=10)
    pubJoint3 = rospy.Publisher('/joint3_position_controller/command', Float64, queue_size=10)

    rospy.init_node('talker', anonymous=True)
    rate = rospy.Rate(10) # 10hz

    letter = predict_Letter('/home/james/Downloads/a.png')
    while not rospy.is_shutdown():

        if(letter == 'a'):
            joint2_angle = 1.3
            rospy.loginfo(joint2_angle)
            pubJoint2.publish(joint2_angle)


            joint3_angle = 0.7
            rospy.loginfo(joint3_angle)
            pubJoint3.publish(joint3_angle)
        else:
            joint2_angle = 0.1
            rospy.loginfo(joint2_angle)
            pubJoint2.publish(joint2_angle)


            joint3_angle = 0.1
            rospy.loginfo(joint3_angle)
            pubJoint3.publish(joint3_angle)
        rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass