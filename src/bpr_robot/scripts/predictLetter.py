#!/usr/bin/env python
import numpy as np 
import matplotlib.pyplot as plt
import cv2
import tensorflow as tf
import keras

def predict_Letter(image):
    IMG_SIZE = 28
    model = keras.models.load_model('/home/james/button_pressing_robot/my_model')
    # Get custom image to predict
    img = image
  
    # Convert to grayscale (100x100x3) -> (100x100)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # Resize to 28x28
    gray = cv2.resize(gray, (IMG_SIZE,IMG_SIZE),interpolation = cv2.INTER_AREA)

    #normalize between 0 and 1
    newimg = tf.keras.utils.normalize(gray, axis = 1)

    newimg = np.array(newimg).reshape(-1, IMG_SIZE, IMG_SIZE, 1)

    newPrediction = model.predict(newimg)

    return number2letter(np.argmax(newPrediction))

alphabetDict = {
    0:'a',
    1:'b',
    2:'c',
    3:'d',
    4:'e',
    5:'f',
    6:'g',
    7:'h',
    8:'i',
    9:'j',
    10:'k',
    11:'l',
    12:'m',
    13:'n',
    14:'o',
    15:'p',
    16:'q',
    17:'r',
    18:'s',
    19:'t',
    20:'u',
    21:'v',
    22:'w',
    23:'x',
    24:'y',
    25:'z',
}

def number2letter(letterIndex):
    print(alphabetDict[letterIndex])
    return alphabetDict[letterIndex]
