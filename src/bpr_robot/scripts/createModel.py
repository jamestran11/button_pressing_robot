#!/usr/bin/env python
import numpy as np 
import pandas as pd 
import matplotlib.pyplot as plt
import cv2
import tensorflow as tf
from keras.models import Sequential
from keras.layers import Dense, Activation, Flatten, Conv2D, MaxPooling2D

IMG_SIZE = 28

# Read emnist letter dataset (train data and test data)
train = pd.read_csv('src/data/emnist-letters-train.csv', header=None)
test = pd.read_csv('src/data/emnist-letters-test.csv', header=None)

# Get relevant columns
train_data = train.iloc[:, 1:] # TRAIN IMAGES - 88000x784 matrix, each row is a flattened 28x28 image
train_labels = train.iloc[:, 0] # TRAIN LABEL - 88000 list of integers from 0-25 denoting a letter from the alphabet
test_data = test.iloc[:, 1:]
test_labels = test.iloc[:, 0]

train_labels = pd.get_dummies(train_labels)
test_labels = pd.get_dummies(test_labels)

# Get values
train_data = train_data.values #88800 elements
train_labels = train_labels.values
test_data = test_data.values
test_labels = test_labels.values

# Delete train and test to free up memory
del train, test

# Dataset comes in sideways, need to rotate
def rotate(image):
    image = image.reshape([IMG_SIZE, IMG_SIZE])
    image = np.fliplr(image)
    image = np.rot90(image)
    return image.reshape([IMG_SIZE * IMG_SIZE])
train_data = np.apply_along_axis(rotate, 1, train_data)
test_data = np.apply_along_axis(rotate, 1, test_data)


# Normalize values of image between 0 and 1
# Train_data.shape = (88800, 784)
train_data = tf.keras.utils.normalize(train_data, axis = 1)
test_data = tf.keras.utils.normalize(test_data, axis = 1)

# Unflatten 88000x784 image to 88000x28x28x1
imageShapedTrain_data = np.array(train_data).reshape(-1, IMG_SIZE, IMG_SIZE, 1)
imageShapedTest_data = np.array(test_data).reshape(-1, IMG_SIZE, IMG_SIZE, 1)

def create_model():
    # Start creating model
    model = Sequential()

    # Input_shape = (28,28,1)
    model.add(Conv2D(28, (3,3), input_shape = imageShapedTrain_data.shape[1:]))
    model.add(Activation("relu"))
    model.add(MaxPooling2D(pool_size=(2,2)))

    model.add(Conv2D(28, (3,3)))
    model.add(Activation("relu"))
    model.add(MaxPooling2D(pool_size=(2,2)))

    model.add(Flatten())
    model.add(Dense(64))
    model.add(Activation("relu"))

    model.add(Dense(32))
    model.add(Activation("relu"))

    model.add(Dense(26))  #26 for number of letters in alphabet
    model.add(Activation("softmax"))

    # Print out neural network summary
    model.summary()
    model.compile(loss = "categorical_crossentropy", optimizer = "adam", metrics=["accuracy"])
    model.fit(imageShapedTrain_data, train_labels, epochs=5, validation_split = 0.1)
    return model

model = create_model()
model.save("my_model")