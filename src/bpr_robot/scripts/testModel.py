#!/usr/bin/env python
import numpy as np 
import matplotlib.pyplot as plt
import cv2
import tensorflow as tf
import keras

# Predicts a letter from the image path given
def predict_Letter(pathToImage):
    IMG_SIZE = 28
    # Load previously generated model.
    # If this does not exist, run createModel.py
    model = keras.models.load_model('./my_model')
    # Get custom image to predict
    img = cv2.imread(pathToImage)

    # Show original image
    cv2.imshow("original image", img)

    # Convert to grayscale (100x100x3) -> (100x100)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # Resize to 28x28
    gray = cv2.resize(gray, (IMG_SIZE,IMG_SIZE),interpolation = cv2.INTER_AREA)

    # Show grayscaled image
    plt.imshow(gray)
    plt.show()

    # Normalize between 0 and 1
    newimg = tf.keras.utils.normalize(gray, axis = 1)

    # Show normalized image
    plt.imshow(newimg)
    plt.show()

    newimg = np.array(newimg).reshape(-1, IMG_SIZE, IMG_SIZE, 1)

    # Vector of 26 elements representing the alphabet
    # Highest value is the prediction
    newPrediction = model.predict(newimg)

    return number2letter(np.argmax(newPrediction))

alphabetDict = {
    0:'a',
    1:'b',
    2:'c',
    3:'d',
    4:'e',
    5:'f',
    6:'g',
    7:'h',
    8:'i',
    9:'j',
    10:'k',
    11:'l',
    12:'m',
    13:'n',
    14:'o',
    15:'p',
    16:'q',
    17:'r',
    18:'s',
    19:'t',
    20:'u',
    21:'v',
    22:'w',
    23:'x',
    24:'y',
    25:'z',
}

# Return corresponding letter 
def number2letter(letterIndex):
    print(alphabetDict[letterIndex])
    return alphabetDict[letterIndex]

# Example
predict_Letter('src/bpr_gazebo/models/key/materials/textures/y.png')