![Semantic description of image](bpr.png "Typing Robot")
![Semantic description of image](keyboardROIs.png "From the POV of the robot")
<details><summary>Notes</summary>

Make sure to have joint_state_publisher and a bunch of other stuff installed  
    `sudo apt install ros-melodic-effort-controllers`  
    `sudo apt install ros-melodic-position-controllers`  

All commands should be ran from catkin workspace

**FLOW: Ran from catkin workspace**  
    `roslaunch bpr_robot all.launch`  
    `rostopic pub -1 /joint1_position_controller/command std_msgs/Float64 -- "0.9"`  

To open Gazebo with BPR robot and table:  
    Need to export gazebo model path first  
    `roslaunch bpr_description spawn.launch`  

To open RViz with BPR robot:  
    `roslaunch bpr_description rviz.launch`  

To create model:  
    `rosrun bpr_robot createModel.py`  

To test model:  
    `rosrun bpr_robot testModel.py`  
    Inside testModel.py, specify the path to the image you want to test  

To check out camera:  
    `roslaunch bpr_description spawn.launch`  
    In diff terminal  
    `rqt_image_view`  
    In diff terminal  
    `rostopic pub -1 /joint2_position_controller/command std_msgs/Float64 "0.5"`  
    `rostopic pub -1 /joint3_position_controller/command std_msgs/Float64 "2.5"`  
    `rostopic pub -1 /joint1_position_controller/command std_msgs/Float64 -- "0.9"`  
    (need -- to publish negative numbers)  
    etc  

To see warped images depending on angle:  
    need spawn.launch launched  
    `rosrun bpr_robot warpPerspective.py`  
    In terminal:  
        - `rostopic pub -1 /joint1_position_controller/command std_msgs/Float64 -- "-1.5"`  
    In another terminal:  
        - `rqt_image_view`  
        - Click on correct topic

To view letters detected in view:  
    `rostopic echo /ros_out`  
    need all.launch to be launched and joint1 to move 
</details>

<details><summary>Issues</summary>  
Common Issues:  
    - If ROS package is not found, make sure you `source devel/setup.bash`  
    - Letters dont show  
        - `export GAZEBO_MODEL_PATH=$GAZEBO_MODEL_PATH:/home/james/bprRepo/button_pressing_robot/src/bpr_gazebo/models` 


Things I had to do to get letters on gazebo models  
- add path to gazebo model path  
    - `export GAZEBO_MODEL_PATH=$GAZEBO_MODEL_PATH:/home/james/bprRepo/button_pressing_robot/src/bpr_gazebo/models`  
- create scripts and textures directory somewhere under /button_pressing_robot/src/bpr_gazebo/models  
    - scripts directory contain material files which are all very similar  
    - textures directory contain png files i wish to add as textures on gazebo models, in this case, handwritten letters   
    - official examples can be found under ~/.gazebo/models  
        - each model contains config file, sdf file (the description of robot, collision/visual/etc), and materials directory, which has scripts and texture directories as well  

- edit table.world  
    - under each model's material tag, I replace the model's top wood plank to reference a different material file  
    - i could probably extract the models defined in the world file and create separate files fo them, then reference them in the world file to make it more modular  


Issue: Robot arm is jittery, swaying  
Solution: Tune PID controller for 3 joints  

Issues to fix:  
Gazebo model sometimes load with slightly varying position, can tell from camera
</details>

<details><summary>Installation</summary>  

Python 2.7  
Pip 20.3.4  
ROS Melodic  
Tensorflow 1.5.0  
Keras 2.2.4  
GAZEBO 9.18.0  

**INSTALL ROS MELODIC**  
sudo apt install ros-melodic-desktop-full  
echo "source /opt/ros/melodic/setup.bash" >> ~/.bashrc  
source ~/.bashrc  
sudo apt install ros-melodic-effort-controllers  
(http://wiki.ros.org/melodic/Installation/Ubuntu)

**UPGRADE GAZEBO from 9.0.0 -> 9.18.0**  
sudo apt-get update && sudo apt-get dist-upgrade

**ADD TO GAZEBO MODEL PATH**  
export GAZEBO_MODEL_PATH= \<catkin workspace path\>/src/bpr_gazebo/models

**INSTALL SOME MORE STUFF**  
python -m pip install -U pip  
pip install tensorflow==1.5  
pip install keras==2.2.4  
pip install pandas  

**BUILD FROM CATKIN_WORKSPACE**  
catkin_make  


</details>

